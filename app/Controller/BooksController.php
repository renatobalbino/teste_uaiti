<?php
class BooksController extends AppController {
	
	public $name = 'Books';
	
	public function index() {
		// desativando o autolayout e autorender para retornar o json dos resultados
		$this->autoRender = false;
		$this->autoLayout = false;
		header('Content-type: application/json');
		
		// seta os campos que serão exibidos como padrão
		$options['fields'] = array('Book.title','Book.author');
		// ordem dos dados para retornar do banco
		$options['order'] = array('Book.title' => 'ASC');
		
		// pega o tamanho da url para depois remover querystring e sobrar apenas os parâmetros
		$url_size = strlen($this->webroot . $this->params['controller']) + 1;
		$request_uri = $this->here;
		$params = explode("/", substr($request_uri, $url_size));
		$vars = array();
		
		// pegando os filtros passados pela url
		foreach($params as $i => $p) {
			$vars[] = explode(':', $p);
		}
		
		$json_books = null;
		
		// se foi passado um id, busca apenas pelo id
		if(isset($this->passedArgs[0])) {
			$options['conditions'] = array('Book.id' => addslashes($this->params['pass'][0]));
			$options['fields'] = array('Book.title','Book.author','Book.isbn','Book.publisher');
			
			// executa a busca no banco
			$books = $this->Book->find('all', $options);
			
			// se não obtiver resultado, exibir uma mensagem informando
			if(empty($books)) {
				$json_books = array("Sua busca não retornou resultados");
			}
			// formando o objeto json como pedido pela tarefa
			else {
				foreach($books as $i => $item) {
					$json_books[] = array(
						'title' => utf8_encode($item['Book']['title']),
						'author' => utf8_encode($item['Book']['author']),
						'isbn' => utf8_encode($item['Book']['isbn']),
						'publisher' => utf8_encode($item['Book']['publisher'])
					);
				}
			}
		}
		// se não, busca pelos filtros 
		else {
			$param = null;
			if(! empty($vars[0][0])) {
				foreach($vars as $i => $v) {
					if(isset($v)) {
						$value = addslashes(str_replace('+', ' ', $v[1]));
						$param .= "Book.{$v[0]} LIKE '%{$value}%'";
					}
					
					if(isset($vars[@$i + 1])) $param .= " OR ";
				}
				$options['conditions'] = $param;
			}
			
			// executa a busca no banco
			$books = $this->Book->find('all', $options);
			
			// se não obtiver resultado, exibir uma mensagem informando
			if(empty($books)) {
				$json_books = array("Sua busca não retornou resultados");
			}
			// formando o objeto json como pedido pela tarefa
			else {
				foreach($books as $i => $item) {
					$json_books[] = array(
						'title' => utf8_encode($item['Book']['title']),
						'author' => utf8_encode($item['Book']['author'])
					);
				}
			}
		}
		
		
		// exibindo na tela o resultado final
		echo json_encode($json_books);
		
	}
	
}